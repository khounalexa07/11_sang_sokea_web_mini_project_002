import React from "react";
import { Button, Card, Col, Container, Image, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";
import "../css/styles.css";

export default function ViewComponent() {
  const location = useLocation();
  const oldData = location.state;
  const navigate = useNavigate();
  return (
    <>
      <Container className="my-2">
        <Row className="view-card p-3">
          <Button
            onClick={() => navigate("/")}
            style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"
          >
            Back
          </Button>
          <div className="" style={{ display: "flex", margin: 5 }}>
            <Image
              src={oldData.image}
              width="50%"
              height="100%"
              style={{ flex: 1, borderRadius: "20px" }}
              className=""
            />
            <div style={{ flex: 1, height: "100%" }}>
              <h4 className="p-3">{oldData.title}</h4>
              <p className="p-3">{oldData.description}</p>
            </div>
          </div>
        </Row>
      </Container>
    </>
  );
}
