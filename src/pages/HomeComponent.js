import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { api } from "../api/api";
import CardComponent from "../component/CardComponent";

export default function HomeComponent() {
  const [data, setData] = useState([]);
  useEffect(() => {
    api.get("/articles").then((res) => {
      setData(res.data.payload);
      console.log(res);
    });
  }, []);

  const handleDelete = (id) => {
    const newData = data.filter((data) => data._id !== id);
    setData(newData);
    api.delete(`/articles/${id}`).then((r) => {
      console.log(r.data.message);
    });
  };

  const navigate = useNavigate();

  return (
    <div>
      <Container>
        <div className="d-flex p-3 justify-content-between">
          <h1>All Articles</h1>
          <Button
            as={Link}
            to="/create"
            className="btn-light text-center align-self-center fw-bold"
          >
            New Article
          </Button>
        </div>
        <Row>
          {data.map((item, index) => (
            <Col xs={6} sm={3} md={2} key={index}>
              <CardComponent item={item} handleDelete={handleDelete} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
