import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "../css/styles.css";
export default function CardComponent({ item, handleDelete, handleUpdate }) {
  const navigate = useNavigate();
  const onUpdate = () => {
    navigate("/update", { state: { ...item } });
  };

  const onView = () => {
    navigate("/view", { state: { ...item } });
  };

  return (
    <>
      <Card className="my-2 " style={{ borderRadius: "20px" }}>
        <Card.Img
          variant="top"
          style={{ height: "200px", objectFit: "cover", borderRadius: "20px" }}
          className="mb-2 p-2"
          src={
            item.image ??
            "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"
          }
        />
        <Card.Body>
          <Card.Title style={{ height: "50px" }} className="title">
            {item.title}
          </Card.Title>
          <Card.Text className="text" style={{ height: "100px" }}>
            {item.description ?? "No info"}
          </Card.Text>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={onView} variant="primary">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => handleDelete(item._id)}
            >
              Delete
            </Button>
            <Button variant="info" onClick={() => onUpdate(item._id)}>
              Update
            </Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}
