import React from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../css/styles.css";
export default function NavBarComponent() {
  return (
    <div>
      <Navbar bg="secondary mb-3" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            className="d-flex justify-content-end"
            id="basic-navbar-nav"
          >
            <Nav className="">
              <Nav.Link className="fw-bold mx-2" as={Link} to="/">
                Home
              </Nav.Link>

              <Nav.Link className="fw-bold" as={Link} to="/category">
                Category
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
