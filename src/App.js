import logo from "./logo.svg";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import HomeComponent from "./pages/HomeComponent";
import NavBarComponent from "./component/NavBarComponent";
import CreatePostComponent from "./pages/CreatePostComponent";
import UpdateComponent from "./pages/UpdateComponent";
import ViewComponent from "./pages/ViewComponent";
import CategoryComponent from "./pages/CategoryComponent";

function App() {
  return (
    <div className="App">
      <NavBarComponent />
      <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="/create" element={<CreatePostComponent />} />
        <Route path="/category" element={<CategoryComponent />} />
        <Route path="/update" element={<UpdateComponent />} />
        <Route path="/view" element={<ViewComponent />} />
      </Routes>
    </div>
  );
}

export default App;
